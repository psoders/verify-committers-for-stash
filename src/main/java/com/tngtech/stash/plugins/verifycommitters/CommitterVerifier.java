package com.tngtech.stash.plugins.verifycommitters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.lang.StringUtils;

import com.atlassian.stash.commit.CommitService;
import com.atlassian.stash.content.Changeset;
import com.atlassian.stash.content.ChangesetCallback;
import com.atlassian.stash.content.ChangesetContext;
import com.atlassian.stash.content.ChangesetSummary;
import com.atlassian.stash.content.ChangesetsBetweenRequest;
import com.atlassian.stash.hook.HookResponse;
import com.atlassian.stash.hook.repository.PreReceiveRepositoryHook;
import com.atlassian.stash.hook.repository.RepositoryHookContext;
import com.atlassian.stash.repository.RefChange;
import com.atlassian.stash.scm.CommandOutputHandler;
import com.atlassian.stash.scm.git.GitCommandBuilderFactory;
import com.atlassian.stash.user.StashUser;
import com.atlassian.stash.user.UserService;
import com.atlassian.utils.process.ProcessException;
import com.atlassian.utils.process.Watchdog;

public class CommitterVerifier implements PreReceiveRepositoryHook {
	private final CommitService commitService;
	private final UserService userService;
	private final GitCommandBuilderFactory commandFactory;

	public CommitterVerifier(CommitService commitService,
			UserService userService, GitCommandBuilderFactory gitCommandBuilderFactory) {
		this.commitService = commitService;
		this.userService = userService;
		this.commandFactory = gitCommandBuilderFactory;
	}

	@Override
	public boolean onReceive(RepositoryHookContext context,	Collection<RefChange> refChanges, HookResponse hookResponse) {
		for (RefChange refChange : refChanges) {
			if (!checkRef(context, refChange, hookResponse))
				return false;
		}
		hookResponse.out().println("Everything looks fine. You may pass.");
		return true;
	}

	private boolean checkRef(RepositoryHookContext context, RefChange refChange, HookResponse hookResponse) {
		for (Changeset changeset : getChanges(context, refChange)) {
			if (!checkChange(changeset, hookResponse)) return false;
		}
		return true;
	}

	private boolean checkChange(Changeset changeset, HookResponse hookResponse) {
		Persons p=getPersons(changeset);
		return validate(p.authorName,p.authorEmail, hookResponse)
				&& validate(p.committerName,p.committerEmail, hookResponse);
	}

	private boolean validate(String userName, String emailAddress, HookResponse hookResponse) {
		hookResponse.out().println("checking: \""+userName+"\" <"+emailAddress+">");

		StashUser user = userService.findUserByNameOrEmail(emailAddress);
		if (user == null) {
			hookResponse.out().println( "Email <" + emailAddress + "> not known to Stash.");
			return false;
		}
		if (!user.getEmailAddress().equals(emailAddress)) {
			hookResponse.out().println( "Incorrect email address: <" + emailAddress + "> instead of <" + user.getEmailAddress() + ">.");
			return false;
		}
		if (!user.getDisplayName().equals(userName)) {
			hookResponse.out().println( "Incorrect user name: <" + userName + "> instead of <" + user.getDisplayName() + ">.");
			return false;
		}
		return true;
	}

	private Iterable<Changeset> getChanges(RepositoryHookContext context, RefChange refChange) {
		ChangesetsBetweenRequest b = new ChangesetsBetweenRequest.Builder(context.getRepository())
		.exclude(refChange.getFromHash())
		.include(refChange.getToHash())
		.build();
		MyCallback callback=new MyCallback();
		commitService.streamChangesetsBetween(b, callback);
		return callback.getValues();
	}

	private Persons getPersons(Changeset changeset) {
		return commandFactory.builder(changeset.getRepository()).revList()
				.format(Persons.GIT_FORMAT).limit(1).rev(changeset.getId())
				.build(new CommitterHandler()).call();
	}

	class MyCallback implements ChangesetCallback{
		List<Changeset> changes = new ArrayList<Changeset>();
		@Override
		public boolean onChangeset(@Nonnull Changeset change) throws IOException {
			changes.add(change); // TODO check directly at this place?
			return true;
		}

		public Iterable<Changeset> getValues() {
			return changes;
		}

		@Override
		public void onEnd(ChangesetSummary arg0) throws IOException {
		}

		@Override
		public void onStart(ChangesetContext arg0) throws IOException {
		}
	}

	class Persons {
		public static final String GIT_FORMAT="%cn%x02%ce%x02%an%x02%ae";

		public String committerName;
		public String committerEmail;
		public String authorName;
		public String authorEmail;

		public Persons(String[] pieces) {
			committerName=pieces[0];
			committerEmail=pieces[1];
			authorName=pieces[2];
			authorEmail=pieces[3];
		}
	}

	class CommitterHandler implements CommandOutputHandler<Persons> {
		private Persons persons;

		@Override
		public void process(InputStream output) throws ProcessException {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					output));
			try {
				String line = reader.readLine();
				if (line == null) {
					return;
				}
				if (!line.startsWith("commit")) {
					throw new IllegalStateException( "[" + line + "]: Unexpected output; expected a 'commit' object");
				}
				line = reader.readLine();
				if (line == null) {
					throw new IllegalStateException("Unexpected end of output; no changeset details were present");
				}
				String[] pieces = StringUtils.splitPreserveAllTokens(line, '\u0002');
				if (pieces.length != 4) {
					throw new IllegalStateException("Unexpected number of pieces found.");
				}
				persons=new Persons(pieces);
			} catch (IOException e) {
				throw new IllegalStateException("read error.");
			}
		}

		@Override
		@Nullable
		public Persons getOutput() {
			return persons;
		}

		@Override
		public void complete() throws ProcessException {}

		@Override
		public void setWatchdog(Watchdog watchdog) {}
	}
}
